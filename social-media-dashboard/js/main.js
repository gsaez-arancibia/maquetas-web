(function() {
  const checkBox = document.getElementById('checkbox');
  const html = document.getElementsByTagName('html')[0];
  
  checkBox.addEventListener('change', (event) => {
    if (event.target.checked) {
      // Dark Mode
      html.setAttribute('data-theme', 'dark');
    } else {
      // Light Mode
      html.setAttribute('data-theme', 'light');
    }
  });
})();