# Maquetas Web

Repositorio que contiene proyectos que fueron realizados con el objetivo de practicar el maquetado web.

## Sitios

En este repositorio se encuentra el código de los siguientes sitios:

* [Headphones](https://mystifying-ardinghelli-65645a.netlify.app/)
* [Social Media Dashboard](https://mystifying-brattain-191bcb.netlify.app/)
* [Unicorn Web](https://suspicious-fermi-452324.netlify.app/)
* [Blog de Café](https://nervous-lalande-9bb400.netlify.app/)
* [Bienes Raíces](https://hopeful-darwin-72d546.netlify.app/)

_Sitios construidos con HTML5, CSS3 Y JAVASCRIPT_

